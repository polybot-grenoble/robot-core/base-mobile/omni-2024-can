# Position asservisment

## Principle explanation

Our position asservisment is based onto two shorts Youtube videos:
- [One about the seeking behaviour](https://www.youtube.com/watch?v=p1Ws1ZhG36g)
- [Another about the arrival behavior](https://www.youtube.com/watch?v=OxHJ-o_bbzs)

## Implementation

As every asservisment in this project, we have to start point for execution, the asservisment constructor and the virtual update method. Here is the list of steps performed in these two methods:

> `constructor(…)`
> * Store references (pointers) to motors and odometry.
> * Store target position, speed_max, acc_max, angular_speed_max and angular_acc_max.

> `update()`
> * Command
>   1. Compute desired speed to reach the target at max speed or arrival speed.
>   2. Compute steering force from the difference between desired and current speeds.
>   3. Cap steering to a max delta speed (acc_max and angular_acc_max times delta_time).
>   4. Apply command.
> * Response if not responded yet.
>   * Send response if near position reached at near zero speed.
