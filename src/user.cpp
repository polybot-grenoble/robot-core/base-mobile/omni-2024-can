#include <string>
#include <vector>

#include "base_mobile/asservisment.hpp"
#include "base_mobile/asservisment_history.hpp"
#include "base_mobile/debug.hpp"
#include "base_mobile/motor.cpp"
#include "base_mobile/odometrie.hpp"
#include "core/core.hpp"
#include "repeating_timer.hpp"

extern volatile bool obstacle_here;

enum Commands {
    CMD_UNPLUG_MOTORS = 0,
    CMD_STOP = 1,
    CMD_AIM_SPEED_RELATIV = 2,
    CMD_AIM_SPEED_ABSOLUTE = 3,
    CMD_GO_TO_RELATIV = 4,
    CMD_GO_TO_ABSOLUTE = 5,
    CMD_GET_ABSOLUTE_POSITION = 6,
    CMD_SET_ABSOLUTE_POSITION = 7,
    CMD_GET_RELATIV_SPEED = 8,
    CMD_GET_ABSOLUTE_SPEED = 9,
    CMD_RESUME = 10,
    CMD_STOP_OSBTACLE = 11,
    CMD_RESUME_OSBTACLE = 13,
    CMD_IS_STOP_OSBTACLE = 12,
    CMD_STOP_RESPOND = 14,
};

std::vector<MotorEncoderWheel*> motors;
std::vector<MotorPosition*> motors_position;

Odometry* odometry;

RepeatingTimer* repeating_timer;

const float ENCODOR_STEP_PER_REV = 1200;
const mm WHEEL_DIAMETER = 58.3;
const float KP = 8, KI = 8, KD = .4, MAX_I_WEIGHT = 255;

void Talos_initialisation() {
    coreSelfCheck->disable();
    Core_setMinimumLoopPeriod((micro_second)5e3);

    // Motor 1
    motors.push_back(new MotorEncoderWheel(PA1, PA3, PA8, PB7, PB0, ENCODOR_STEP_PER_REV,
                                           WHEEL_DIAMETER, KP, KI, KD, MAX_I_WEIGHT));
    motors_position.push_back(new MotorPosition(124, 18, -PI / 2));

    // Motor 2
    motors.push_back(new MotorEncoderWheel(PA4, PA7, PA10, PB6, PB1, ENCODOR_STEP_PER_REV,
                                           WHEEL_DIAMETER, KP, KI, KD, MAX_I_WEIGHT));
    motors_position.push_back(
        new MotorPosition(motors_position[0]->rotate_from_origin(TWO_PI / 3)));

    // Moteur 3
    motors.push_back(new MotorEncoderWheel(PB3, PA0, PA9, PB4, PB5, ENCODOR_STEP_PER_REV,
                                           WHEEL_DIAMETER, KP, KI, KD, MAX_I_WEIGHT));
    motors_position.push_back(
        new MotorPosition(motors_position[0]->rotate_from_origin(2 * TWO_PI / 3)));

    /* ODOMETRY = Motor's list , Motors' positions */
    odometry = new Odometry(motors, motors_position);
    // odometry->set_position_absolute(Position(0, 0, 0));

    obstacle_here = false;
    asservisment_new_set(new AsservUnplug(motors));
    // asservisment_new_set(new AsservSpeedRelative(motors, odometry, Speed(0, WHEEL_DIAMETER * PI * 2, 0)));
    // asservisment_new_set(new AsservSpeedRelative(motors, odometry, Speed(0, WHEEL_DIAMETER*PI*2, PI/3)));
    // asservisment_new_set(new AsservSpeedAbsolute(motors, odometry, Speed(0, WHEEL_DIAMETER*PI*2, TWO_PI/5)));
    // asservisment_new_set(new AsservPositionAbsolute(motors, odometry, Position(150, 150, 0), nullptr, 0, 0));
    // asservisment_new_set(new AsservPositionAbsolute(motors, odometry, Position(0, 0, PI/2), nullptr, 0, 0));
    // asservisment_new_set(new AsservPositionAbsolute(motors, odometry, Position(300, -300, 0), nullptr, 0, 0));
    asservisment_new_set(new AsservPositionAbsolute(motors, odometry, Position(0, 1000, 0), nullptr, 0, 0));
    // asservisment_new_set(new AsservPositionAbsolute(motors, odometry, Position(0, 0, PI/2), nullptr, 0, 0));
}

void Talos_loop() {
    odometry->update();

    Serial.print(";Pos=");
    odometry->get_position_absolute().print();

    AsservismentAbstract* current_asserve = asservisment_current_get();
    if (current_asserve != nullptr)
        current_asserve->update();

    Serial.println("");
}

void Talos_onError() {
    asservisment_new_set(new AsservUnplug(motors), true);
}

void Core_priorityLoop() {}

void Hermes_onMessage(Hermes_t* hermes_instance, uint8_t sender, uint16_t command, uint32_t isResponse) {
    /**
     * Code to be run every time a message is received over the CAN network.
     */
    if (command == 4095)
        return;

    HermesBuffer* buf;
    if (Hermes_getInputBuffer(hermes_instance, sender, command, &buf) != HERMES_OK) {
        return;
    }

    Core_log("Message " + std::to_string(command));

    switch ((Commands)command) {
        case CMD_UNPLUG_MOTORS: {
            asservisment_new_set(new AsservUnplug(motors));
            break;
        }
        case CMD_STOP: {
            asservisment_new_set(new AsservSpeedZero(motors));
            break;
        }
        case CMD_STOP_RESPOND:
            asservisment_new_set(new AsservSpeedZeroResponse(
                motors, odometry, hermes_instance, sender, command));
            break;
        case CMD_AIM_SPEED_RELATIV: {
            float x, y, theta;
            Hermes_rewind(buf);
            Hermes_get(buf, &x, &y, &theta);
            asservisment_new_set(
                new AsservSpeedRelative(motors, odometry, Speed(x, y, theta)));
            break;
        }
        case CMD_AIM_SPEED_ABSOLUTE: {
            float x, y, theta;
            Hermes_rewind(buf);
            Hermes_get(buf, &x, &y, &theta);
            asservisment_new_set(
                new AsservSpeedAbsolute(motors, odometry, Speed(x, y, theta)));
            break;
        }
        case CMD_GO_TO_RELATIV: {
            float x, y, theta;
            Hermes_rewind(buf);
            Hermes_get(buf, &x, &y, &theta);
            // TODO : a compléter avec l'odometrie

            // Cette ligne envoie une erreur à Core pour faire sauter une
            // quelconque stratégie qui tentrait d'utiliser cette commande
            // inexistante. Merci de la retirer une fois la commande implémentée
            Hermes_error(hermes_instance, sender, HERMES_ERR_INVALID_COMMAND_ID, command);
            break;
        }
        case CMD_GO_TO_ABSOLUTE: {
            Core_log("CMD_GO_TO_ABSOLUTE");
            float x, y, theta;
            Hermes_rewind(buf);
            Hermes_get(buf, &x, &y, &theta);
            asservisment_new_set(new AsservPositionAbsolute(
                motors, odometry, Position(x, y, theta), hermes_instance, sender, command));
            break;
        }
        case CMD_GET_ABSOLUTE_POSITION: {
            Position absolute_position = odometry->get_position_absolute();
            Hermes_send(hermes_instance, sender, command, true, "fff",
                        absolute_position.x_y.x, absolute_position.x_y.y,
                        absolute_position.teta);
            break;
        }
        case CMD_SET_ABSOLUTE_POSITION: {
            float x, y, theta;
            Hermes_rewind(buf);
            Hermes_get(buf, &x, &y, &theta);
            odometry->set_position_absolute(Position(x, y, theta));
            break;
        }
        case CMD_GET_RELATIV_SPEED: {
            Speed relative_speed = odometry->get_speed_local();
            Hermes_send(hermes_instance, sender, command, true, "fff",
                        relative_speed.x_y.x, relative_speed.x_y.y,
                        relative_speed.teta);
            break;
        }
        case CMD_GET_ABSOLUTE_SPEED: {
            Speed absolute_speed = odometry->get_speed_absolute();
            Hermes_send(hermes_instance, sender, command, true, "fff",
                        absolute_speed.x_y.x, absolute_speed.x_y.y,
                        absolute_speed.teta);
            break;
        }
        case CMD_RESUME: {
            const bool resume_success = asservisment_previous_resume();
            Hermes_send(hermes_instance, sender, CMD_RESUME, true, "o", resume_success);
            break;
        }
        case CMD_STOP_OSBTACLE:
            if (!obstacle_here)
                asservisment_new_set(new AsservSpeedZeroObstacle(motors));
            obstacle_here = true;
            break;
        case CMD_IS_STOP_OSBTACLE: {
            Hermes_send(hermes_instance, sender, CMD_IS_STOP_OSBTACLE, true, "o", obstacle_here);
            break;
        }
        case CMD_RESUME_OSBTACLE: {
            if (obstacle_here)
                asservisment_previous_resume(true);
            obstacle_here = false;
            break;
        }
    }
}

void serialEvent() {
    // float params[3];

    // for (size_t param_num = 0; param_num < 3; param_num++) {
    //     std::string buffer = "0";
    //     while (true) {
    //         while (!Serial.available())
    //             ;
    //         char c = Serial.read();
    //         if (c == ';' || c == '\n') break;
    //         buffer += c;
    //     }
    //     params[param_num] = std::stof(buffer);
    // }

    // motor->set_pid_parameters(params[0], params[1], params[2], 255);
}
