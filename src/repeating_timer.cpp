#include "repeating_timer.hpp"

#include <Arduino.h>

RepeatingTimer::RepeatingTimer(const unsigned long period_us)
    : period_us(period_us), time_previous_us(micros() - period_us) {}

bool RepeatingTimer::is_time_up() {
    const unsigned long time_now_us = micros();
    if (time_now_us >= time_previous_us + period_us) {
        time_previous_us = time_now_us;
        return true;
    } else {
        return false;
    }
}
