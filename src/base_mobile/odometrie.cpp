#include "base_mobile/odometrie.hpp"

#include <iterator>
#include <ranges>
#include <tuple>

// Coef matthieu pour une base mobile à 3 roues
//  const float coef_M1_x = cos(PI/2); //0
//  const float coef_M2_x = cos(PI/2 + 2*PI/3);
//  const float coef_M3_x = cos(PI/2 + 4*PI/3);

// const float coef_M1_y = sin(PI/2);
// const float coef_M2_y = sin(PI/2 + 2*PI/3);
// const float coef_M3_y = sin(PI/2 + 4*PI/3);

// const float coef_M1_rot = 1;
// const float coef_M2_rot = 1;
// const float coef_M3_rot = 1;

// const float InfluenceTot_x = abs(coef_M1_x) + abs(coef_M2_x) +
// abs(coef_M3_x); const float InfluenceTot_y = abs(coef_M1_y) + abs(coef_M2_y)
// + abs(coef_M3_y); const float InfluenceTot_rot = abs(coef_M1_rot) +
// abs(coef_M2_rot) + abs(coef_M3_rot);

Odometry::Odometry(std::vector<MotorEncoderWheel*> motors,
                   std::vector<MotorPosition*> motors_positions)
    : motors(motors), motors_positions(motors_positions) {
    position_absolute = Position();
    speed_local = Speed();

    compute_weights();
}

void Odometry::update() {
    // 1. Update motors encoders
    std::for_each(motors.begin(), motors.end(),
                  [](MotorEncoderWheel* const motor) {
                      motor->pop_encoder_count_period();
                      //   Serial.print("motor :");
                      //   Serial.print(motor->get_rot_last_period(),5);
                      //   Serial.print("\n");
                      //   printf("motor :
                      //   %0.4f\n",motor->get_rot_last_period());
                  });

    // 2. Get each motors contribution
    std::vector<Contribution> contributions;
    std::transform(motors.begin(), motors.end(), back_inserter(contributions),
                   [](MotorEncoderWheel* const motor) {
                       return Contribution{
                           .travelled_distance =
                               motor->get_traveled_distance_last_period(),
                           .speed = motor->get_speed_last_period()};
                   });
    // 3. resolve to local movement (and save speed)
    Position local_movement_per_axis = resolve_local_odometry(contributions);
    // if (local_movement_per_axis.teta == 0) {
    //     position_absolute.x_y +=
    //         local_movement_per_axis.x_y.rotate(position_absolute.teta);

    // } else {
    //     Position local_movement_interpolated;

    //     // circular arc interpolation
    //     mm R = local_movement_per_axis.get_abs() /
    //     local_movement_per_axis.teta; local_movement_interpolated.x_y.x = R *
    //     sin(local_movement_per_axis.teta); local_movement_interpolated.x_y.y
    //     = R * (1 - cos(local_movement_per_axis.teta));

    //     rad angle_at_start = local_movement_per_axis.x_y.angle();

    //     local_movement_interpolated =
    //         local_movement_interpolated.rotate_from_origin(angle_at_start);
    //     local_movement_interpolated.teta = local_movement_per_axis.teta;

    // position_absolute += local_movement_interpolated;
    // }

    // Distance local_movement_interpolated;
    // float theta = local_movement_per_axis.teta;
    // if (abs(theta) < 1.0) {
    //     /**
    //      * Developpement limité:
    //      *  Fonction en x => x'=|xy| * (theta/2 - theta^3/24 + theta^5/720)
    //      *  Fonction en y => y'=|xy| * (1 - theta^2/6 - theta^4/120)
    //     */
    //     local_movement_interpolated.x_y.x =
    //         local_movement_per_axis.get_abs() *
    //         ((theta / 2) - (pow(theta, 3) / 24) + (pow(theta, 5) / 720));
    //     local_movement_interpolated.x_y.y =
    //         local_movement_per_axis.get_abs() *
    //         (1 - (pow(theta, 2) / 6) + (pow(theta, 4) / 120));

    // } else {
    //     /**
    //      * Fonction de base:
    //      *  Fonction en x => x'=|xy|*sin(theta)/theta
    //      *  Fonction en y => y'=|xy|*(1-cos(theta))/theta
    //     */
    //     mm L = local_movement_per_axis.get_abs() / theta;
    //     local_movement_interpolated.x_y.x = L * (1 - cos(theta));
    //     local_movement_interpolated.x_y.y = L * sin(theta);
    // }
    // // Ajout du mouvement sur la position global

    // rad angle_at_start = position_absolute.teta;

    // local_movement_interpolated =
    //     local_movement_interpolated.rotate_from_origin(angle_at_start);
    // local_movement_interpolated.teta = local_movement_per_axis.teta;

    // position_absolute += local_movement_interpolated;

    // 4. Propagate movement to absolute
    position_absolute += local_movement_per_axis;
    position_absolute.teta = frame_angle(position_absolute.teta);
}

Speed Odometry::get_speed_local() { return speed_local; }

Speed Odometry::get_speed_absolute() {
    return Speed(speed_local.x_y.rotate(position_absolute.teta),
                 speed_local.teta);
}

Position Odometry::get_position_absolute() { return position_absolute; }

void Odometry::set_position_absolute(Position new_position) {
    position_absolute = new_position;
}

void Odometry::compute_weights() {
    float contribution_sum_x = 0;
    float contribution_sum_y = 0;
    float contribution_sum_teta = 0;

    // Local to contribution weights (Command coefficients)
    std::transform(
        motors_positions.begin(), motors_positions.end(),
        back_inserter(motor_local_to_contribution_weights),
        [](MotorPosition* const motor_position) {
            MotorUnitWeights motor_loc_to_cont_weight = MotorUnitWeights(
                cos(motor_position->teta), sin(motor_position->teta),
                sin(motor_position->teta - motor_position->x_y.angle()) *
                    motor_position->x_y.get_abs());

            return motor_loc_to_cont_weight;
        });

    // Contribution to local weights (Odometry coefficients)
    std::transform(
        motors_positions.begin(), motors_positions.end(),
        back_inserter(motor_contribution_to_local_weights),
        [&contribution_sum_x, &contribution_sum_y,
         &contribution_sum_teta](MotorPosition* const motor_position) {
            MotorUnitWeights motor_cont_to_loc_weight = MotorUnitWeights(
                cos(motor_position->teta), sin(motor_position->teta),
                sin(motor_position->teta - motor_position->x_y.angle()) /
                    motor_position->x_y.get_abs());

            contribution_sum_x += pow(motor_cont_to_loc_weight.x_y.x, 2);
            contribution_sum_y += pow(motor_cont_to_loc_weight.x_y.y, 2);
            contribution_sum_teta += abs(motor_cont_to_loc_weight.teta);

            return motor_cont_to_loc_weight;
        });

    for (size_t motor_index = 0;
         motor_index < motor_contribution_to_local_weights.size();
         motor_index++) {
        motor_contribution_to_local_weights[motor_index].x_y.x /= contribution_sum_x;
        motor_contribution_to_local_weights[motor_index].x_y.y /= contribution_sum_y;
        motor_contribution_to_local_weights[motor_index].teta *=
            abs(motor_contribution_to_local_weights[motor_index].teta) /
            contribution_sum_teta;
    }
}

Position Odometry::resolve_local_odometry(std::vector<Contribution> contributions) {
    Position local_movement = Position();
    speed_local = Speed();  // Reset local speed

    // std::views::zip isn't available on STM because "it doesn't make sense".
    for (size_t motor_index = 0; motor_index < contributions.size();
         motor_index++) {
        Contribution contribution = contributions[motor_index];
        MotorUnitWeights weights =
            motor_contribution_to_local_weights[motor_index];

        // local movement
        local_movement += weights * contribution.travelled_distance;
        // local speed
        speed_local += weights * contribution.speed;
    }

    return local_movement;
}

std::vector<mm_per_s> Odometry::compute_speed_local_to_motors(
    Speed local_speed) {
    std::vector<mm_per_s> contributions;
    std::transform(motor_local_to_contribution_weights.begin(),
                   motor_local_to_contribution_weights.end(),
                   back_inserter(contributions),
                   [local_speed](MotorUnitWeights const motor_weights) {
                       return local_speed.x_y.x * motor_weights.x_y.x +
                              local_speed.x_y.y * motor_weights.x_y.y +
                              local_speed.teta * motor_weights.teta;
                   });

    // Exemple with Eigen vectorized approach
    // motor_local_to_contribution_weights has dimensions,
    // and local_speed has dimensions compatible with the first two elements
    // (x_y) Eigen::VectorXd contributions =
    // local_speed.head(2).cwiseProduct(motor_local_to_contribution_weights.colwise().dot(motor_local_to_contribution_weights.row(2)))
    //  + local_speed(2) *
    //  motor_local_to_contribution_weights.colwise().sum().asDiagonal() *
    //  local_speed(2);

    return contributions;
}

Speed Odometry::speed_local_to_absolute(Speed speed_local) {
    return speed_local.rotate_only_vector(position_absolute.teta);
}
Speed Odometry::speed_absolute_to_local(Speed speed_absolute) {
    return speed_absolute.rotate_only_vector(-position_absolute.teta);
}

void Odometry::print_weights() const {
    Serial.print(
        "Motor contribution to local weight "
        "(motor_contribution_to_local_weights):\n");
    for (int index = 0; index < motor_contribution_to_local_weights.size();
         index++) {
        Serial.print("|");
        motor_contribution_to_local_weights[index].print();
        Serial.print("|\n");
    }
    Serial.print("\n");

    Serial.print(
        "Motor local to contribution weight "
        "(motor_local_to_contribution_weights):\n");
    for (int index = 0; index < motor_local_to_contribution_weights.size();
         index++) {
        Serial.print("|");
        motor_local_to_contribution_weights[index].print();
        Serial.print("|\n");
    }
    Serial.print("\n");
}
