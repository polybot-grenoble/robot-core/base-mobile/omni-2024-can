#include "base_mobile/coordinates.hpp"

rad frame_angle(rad angle) {
    while (angle < PI)
        angle += TWO_PI;
    while (angle > PI)
        angle -= TWO_PI;
    return angle;
}

float set_abs_float(float value, float desired_abs) {
    if (value < 0)
        return -abs(desired_abs);
    if (value > 0)
        return abs(desired_abs);
    return 0;
}
