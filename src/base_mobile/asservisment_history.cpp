#include "base_mobile/asservisment_history.hpp"

#include <vector>

volatile bool obstacle_here;

// Number of asservisment in memory, including current one.
const size_t history_length_max = 1 + 4;

std::vector<AsservismentAbstract*> history;

void trim_history() {
    while (history.size() > history_length_max) {
        delete history.front();
        history.erase(history.begin());
    }
}

AsservismentAbstract* asservisment_current_get() {
    if (history.size() > 0)
        return history.back();
    else
        return nullptr;
}

// Take the given asservisment ownership.
void asservisment_new_set(AsservismentAbstract* asservisment_new, const bool even_with_obstacle) {
    if (obstacle_here && !even_with_obstacle) {
        delete asservisment_new;
        if (history.size() > 0)
            history.back()->resume();
        return;
    }
    history.push_back(asservisment_new);
    trim_history();
}

bool asservisment_previous_resume(const bool even_with_obstacle) {
    if (obstacle_here && !even_with_obstacle) {
        return false;
    }

    if (history.size() > 0) {
        delete history.back();
        history.pop_back();
    }
    if (history.size() > 0) {
        history.back()->resume();
        return true;
    } else {
        return false;
    }
}
