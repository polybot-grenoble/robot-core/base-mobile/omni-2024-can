#include "core/coreGenerics.hpp"

using namespace Core;

/** SELFCHECK **/

SelfCheck::SelfCheck(Hermes_t *hermes) {
    hermesInstance = hermes;

#ifdef CORE_F446RE
    tim = new HardwareTimer(TIM2);
    tim->setPrescaleFactor(18000);
#else
    tim = new HardwareTimer(TIM15);
    tim->setPrescaleFactor(8000);
#endif

    this->reset();

    tim->setMode(1, TIMER_OUTPUT_COMPARE_ACTIVE, NC);
    tim->setOverflow(10 * CORE_SELFCHECK_WAIT_TIME_MS);
    tim->attachInterrupt([this]() { this->callback(); });
    tim->resume();
}

void SelfCheck::reset() {
    canSilent = true;
    processFroze = false;
    requestEscape = false;
}

SelfCheck::~SelfCheck() { this->disable(); }

void SelfCheck::enable() { enabled = true; }

void SelfCheck::disable() { enabled = false; }

void SelfCheck::canCheck() { canSilent = false; }

void SelfCheck::processCheck() { processFroze = false; }

void SelfCheck::callback() {
    if (!enabled) {
        return;
    }

    // Make sure the CAN isn't silent for too long otherwise we don't know if it
    // gets disconnected.
    if (canSilent) {
        Hermes_heartbeat(hermesInstance, DEVICE_ID, false);
    } else {
        canSilent = true;  // Set it to true. It will be set to false by the
                           // main process if TX/RX on CAN.
    }

    if (false) { // DISABLED FOR BAD BEHAVIOUR. TO BE REWORKED.
        requestEscape = true;
        longjmp(jmpLoc, -1);  // Jump to last escape point.
        return;
    } else {
        processFroze = true;  // We assume it is frozen.
    }
}

/** REPEATING TIMER **/

RepeatingTimer::RepeatingTimer(const micro_second period)
    : __period(period), previousTime(micros() - period), enabled(true) {}

RepeatingTimer::RepeatingTimer()
    : __period(0), previousTime(micros()), enabled(false) {}

bool RepeatingTimer::enable() {
    if (enabled || __period == 0) return false;
    enabled = true;
    return true;
}

bool RepeatingTimer::disable() {
    if (!enabled || __period == 0) return false;
    enabled = false;
    return true;
}

void RepeatingTimer::setPeriod(const micro_second period) { __period = period; }

void RepeatingTimer::setFrequency(const hertz frequency) {
    __period = (micro_second)(pow(10, 6) / frequency);
}

bool RepeatingTimer::is_time_up() {
    if (!enabled) return true;
    const micro_second currentTime = micros();
    if (currentTime >= previousTime + __period) {
        previousTime = currentTime;
        return true;
    } else {
        return false;
    }
}
