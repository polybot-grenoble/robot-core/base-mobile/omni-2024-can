# Documentation technique de la base mobile
Cette documentation technique explique chaque classes et méthodes du code de la base mobile. Ce document est classé par fichier présent dans le dossier **omni-2024-can**

* [Documentation technique de la base mobile](#documentation-technique-de-la-base-mobile)
    * [Fichier `Motor.cpp`](#fichier-motor.cpp)
        * [Classe : `Encoder`](#classe--encoder)
        * [Classe : `Motor`](#classe--motor)
        * [Classe : `MotorEncoderWheel`](#classe--motorencoderwheel)
    * [Fichier `Pid.cpp`](#fichier-pid.cpp)
        * [Classe : `PID`](#classe--pid)
    * [Fichier `Asservisment.cpp`](#fichier-asservisment.cpp)
        * [Classe `Asservisment`](#classe-asservisment)
        * [Classe `Unplug` (hérite de `Asservisment`)](#classe-unplug-(hérite-de-asservisment))
        * [Classe `Speed_zero` (hérite de `Asservisment`)](#classe-speed_zero-(hérite-de-asservisment))
        * [Classe `Speed_relative_set` (hérite de `Asservisment`)](#classe-speed_relative_set-(hérite-de-asservisment))
        * [Classe `Speed_absolute_set` (hérite de `Asservisment`)](#classe-speed_absolute_set-(hérite-de-asservisment))
    * [fichier `odometrie.hpp`](#fichier-odometrie.hpp)
        * [Typedefs](#typedefs)
        * [Struct `Contribution`](#struct-contribution)
            * [Membres](#membres)
        * [Classe `Odometry`](#classe-odometry)
    * [Fichier `coordinate.hpp`](#fichier-coordinate.hpp)
        * [Classe `Vector2D`](#classe-vector2d)
        * [Classe `Vector2DAndRotation`](#classe-vector2dandrotation)
    * [Fichier `user.cpp`](#fichier-user.cpp)
        * [variables](#variables)
        * [enum `commands`](#enum-commands)


## Fichier `Motor.cpp`

### Classe : `Encoder`
#### Description
Classe pour gérer un encodeur rotatif. Elle permet de lire les signaux d'un encodeur rotatif et de calculer la vitesse de rotation en tours par minute (RPM) ainsi que le nombre de rotations effectuées sur une période donnée.

#### Membres Privés
- `const pin pin_encoderA, pin_encoderB` : Pins pour les signaux A et B de l'encodeur.
- `const float enc_step_per_revolution` : Nombre de pas de l'encodeur par révolution.

#### Membres Protégés
- `int nb_step_encodeur` : Nombre total de pas comptés par l'encodeur.
- `int enc_step_last_period` : Nombre de pas comptés durant la dernière période.
- `micro_second time_previous` : Temps de la dernière mesure.
- `micro_second time_delta` : Temps écoulé entre deux mesures.
- `bool measure_computed` : Indique si la dernière mesure a été calculée.
- `rpm rpm_last_period_filtered` : RPM filtré de la dernière période.
- `rot rot_last_period` : Nombre de rotations effectuées durant la dernière période.
- `rpm rpm_last_period_previous_unfiltered` : RPM non filtré de la période précédente.

#### Constructeur
```cpp
Encoder(float enc_step_per_revolution, pin pin_encoderA, pin pin_encoderB);
```

#### Méthodes Publiques
- `void handle_enc_interrupt()` : Gère l'interruption de l'encodeur.
- `void pop_encoder_count_period()` : Réinitialise le compteur de pas pour une nouvelle période après l'avoir récupéré.
- `rot get_rot_last_period()` : Retourne le nombre de rotations effectuées durant la dernière période.
- `rpm get_rpm()` : Retourne la vitesse de rotation en RPM de la dernière période.

#### Méthodes Privées
- `void compute_movement()` : Calcule le mouvement basé sur les mesures de l'encodeur et filtre les mesures de vitesse qui sont sensible aux hautes fréquences.

**Explications supplémentaires :**
- `pin_encoderA` et `pin_encoderB` sont utilisés pour lire les signaux de l'encodeur.
- `enc_step_per_revolution` représente le nombre de pas que l'encodeur génère par tour complet de l'axe moteur.
- `handle_enc_interrupt()` est une fonction qui est appelée à chaque interruption générée par le signal de l'encodeur. Elle met à jour le compteur de pas en fonction de la direction de rotation.
- `pop_encoder_count_period()` récupère les données pour le calcul de la vitesse et du nombre de rotations.
- `get_rot_last_period()` et `get_rpm()` sont des fonctions publiques qui retournent respectivement le nombre de rotations et la vitesse en RPM calculées sur la dernière période.
- `compute_movement()` est une fonction privée qui effectue le calcul des rotations et de la vitesse en RPM à partir des données recueillies.

---
---

### Classe : `Motor`

#### Description
Classe conçue pour contrôler un moteur via des signaux PWM.

#### Membres Privés
- `const pin pin_pwm` : Pin utilisé pour le signal PWM.
- `const pin pin_direction_positive` : Pin positif de l'alimentation du moteur.
- `const pin pin_direction_negative` : Pin négatif de l'alimentation du moteur

#### Constructeur
```cpp
Motor(const pin pin_pwm, const pin pin_direction_positive, const pin pin_direction_negative);
```

#### Méthodes Publiques
- `void set_motor_power(int power)` : Règle la puissance du moteur.

#### Exemple d'Utilisation
```cpp
Motor motor(3, 4, 5); // Crée un objet Motor avec les pins 3, 4 et 5.
motor.set_motor_power(150); // Définit la puissance du moteur à 150.
motor.set_motor_power(-150); // Inverse la direction et définit la puissance à 150.
```

**Explications supplémentaires :**
- `set_motor_power(int power)` : Cette méthode prend un paramètre `power` qui peut varier de -255 à 255. Une valeur positive entraîne une rotation dans le sens positif, tandis qu'une valeur négative entraîne une rotation dans le sens négatif.

---
---

### Classe : `MotorEncoderWheel`
Hérite de : `MotorEncoder`

#### Description
Classe dérivée de `MotorEncoder` conçue pour piloter l'ensemble moteur + encodeur + roue de la base mobile.

#### Membres Protégés
- `const mm WHEEL_PERIMETER` : Périmètre de la roue.

#### Constructeur
```cpp
MotorEncoderWheel(pin pin_encoderA, pin pin_encoderB, pin pin_pwm,
                  pin pin_direction_positive, pin pin_direction_negative,
                  float enc_step_per_revolution, mm wheel_diameter,
                  const float KP, const float KI, const float KD,
                  const float max_integral_absolute_weight);
```

#### Méthodes Publiques
- `mm get_traveled_distance_last_period()` : Retourne la distance parcourue durant la dernière période.
- `mm_per_s get_speed_last_period()` : Retourne la vitesse de la dernière période en mm/s.
- `void set_speed(mm_per_s desired_speed)` : Définit la vitesse désirée en mm/s.
- `void update_speed()` : Met à jour la vitesse du moteur.
- `void unplug()` : Déconnecte le moteur.

#### Exemple d'Utilisation
```cpp
MotorEncoderWheel m1(2, 3, 4, 5, 6, 1000, 70, 1.0, 0.1, 0.01, 10);
m1.set_speed(100); // Définit la vitesse désirée à 100 mm/s.
m1.update_speed(); // Met à jour la vitesse du moteur.
```

**Explications supplémentaires :**
- `WHEEL_PERIMETER` est calculé en utilisant la formule $$\pi \times \text{diamètre de la roue}$$.
- `get_traveled_distance_last_period()` utilise le nombre de rotations de la dernière période pour calculer la distance parcourue.
- `get_speed_last_period()` calcule la vitesse en divisant le périmètre de la roue par le temps d'une période.
- `set_speed(mm_per_s desired_speed)` ajuste la vitesse du moteur pour atteindre la vitesse désirée.
- `update_speed()` appelle la méthode `update_rpm()` de la classe parente pour mettre à jour la vitesse du moteur.
- `unplug()` est une méthode héritée qui est utilisée pour déconnecter logiciellement le moteur.

## Fichier `Pid.cpp`

### Classe : `PID`

#### Description
Classe conçue pour implémenter un contrôleur PID (Proportionnel, Intégral, Dérivé) utilisé pour le contrôle de la vitesse des moteurs.

#### Membres Privés
- `float KP, KI, KD` : Coefficients du contrôleur PID.
- `float error_previous` : Erreur précédente enregistrée.
- `float error_integral` : Somme intégrale des erreurs.
- `float max_integral_absolute_weight` : Poids maximal absolu de l'intégrale pour l'anti-windup.

#### Membres Protégés
- `float target` : Cible que le PID essaie d'atteindre.

#### Constructeurs
- `PID()` : Initialise le contrôleur avec des valeurs par défaut.
- `PID(float KP, float KI, float KD)` : Initialise le contrôleur avec des coefficients spécifiés.
- `PID(float KP, float KI, float KD, float max_integral_absolute_weight)` : Initialise le contrôleur avec des coefficients spécifiés et un poids maximal pour l'intégrale.

#### Méthodes Publiques
- `void set_parameters(float KP, float KI, float KD, float max_integral_absolute_weight)` : Définit les paramètres du contrôleur PID.
- `void set_target(float target)` : Définit la cible pour le contrôleur PID.
- `float execute_pid_control(float value_current, micro_second time_delta)` : Exécute le contrôle PID et retourne le signal de commande.

#### Fonctionnement du PID
Le contrôleur PID calcule l'erreur entre la valeur cible et la valeur actuelle, puis applique les trois actions suivantes :
- **Proportionnelle** : $$ KP \times \text{erreur} $$
- **Intégrale** : $$ KI \times \int \text{erreur} \, dt $$
- **Dérivative** : $$ KD \times \frac{d(\text{erreur})}{dt} $$

## Fichier `Asservisment.cpp`

### Classe `Asservisment`
Cette classe de base abstraite représente un système d'asservissement générique.

#### Méthodes
- `virtual void update() {}` : Méthode virtuelle pour mettre à jour l'asservissement.

### Classe `Unplug` (hérite de `Asservisment`)
Cette classe est utilisée pour débrancher logiciellement les moteurs.

#### Constructeur
- `Unplug(std::vector<MotorEncoderWheel*> motors)` : Débranche logiciellement chaque moteur fourni dans le vecteur.

---
---

### Classe `Speed_zero` (hérite de `Asservisment`)
Cette classe définit la vitesse des moteurs à zéro. Permet de s'arrêter rapidement.

#### Membres
- `std::vector<MotorEncoderWheel*> motors` : Vecteur de pointeurs vers les moteurs.

#### Constructeur
- `Speed_zero(std::vector<MotorEncoderWheel*> motors)` : Initialise les moteurs et définit leur vitesse à zéro.

#### Méthodes
- `void update()` : Met à jour la vitesse des moteurs à zéro.

---
---

### Classe `Speed_relative_set` (hérite de `Asservisment`)
Cette classe ajuste la vitesse des moteurs relativement à une vitesse locale.

#### Membres
- `const std::vector<MotorEncoderWheel*> motors` : Vecteur de pointeurs vers les moteurs.

#### Constructeur
- `Speed_relative_set(std::vector<MotorEncoderWheel*> motors, Odometry* odometry, Speed local_speed)` : Calcule et définit la vitesse relative des moteurs.

#### Méthodes
- `void update()` : Met à jour la vitesse locales des moteurs.

---
---

### Classe `Speed_absolute_set` (hérite de `Asservisment`)
Cette classe ajuste la vitesse des moteurs.

#### Membres
- `const std::vector<MotorEncoderWheel*> motors` : Vecteur de pointeurs vers les moteurs.
- `Odometry* const odometry` : Pointeur vers l'odométrie.
- `const Speed absolute_speed` : Vitesse absolue à appliquer.

#### Constructeur
- `Speed_absolute_set(std::vector<MotorEncoderWheel*> motors, Odometry* odometry, Speed absolute_speed)` : Initialise les moteurs avec la vitesse absolue.

#### Méthodes
- `void update()` : Met à jour la vitesse des moteurs.

## fichier `odometrie.hpp`

### Typedefs
- `typedef Position MotorPosition` : Alias pour la position d'un moteur
- `typedef Vector2DAndRotation MotorUnitWeights` : Alias pour les poids unitaires d'un moteur

### Struct `Contribution`
Représente la contribution d'un moteur en termes de distance parcourue et de vitesse.

#### Membres
- `mm travelled_distance` : Distance parcourue par le moteur.
- `mm_per_s speed` : Vitesse du moteur.

---
---

### Classe `Odometry`
Gère l'odométrie, c'est-à-dire la mesure du déplacement du robot.

#### Membres Protégés
- `Position position_absolute` : Position absolue.
- `Speed speed_local` : Vitesse locale.
- `const std::vector<MotorEncoderWheel*> motors` : Vecteur de pointeurs vers les moteurs.
- `const std::vector<MotorPosition*> motors_positions` : Vecteur de pointeurs vers les positions des moteurs.
- `std::vector<MotorUnitWeights> motor_contribution_to_local_weights` : Coefficients d'odométrie pour convertir le mouvement des roues en mouvement local pondéré.
- `std::vector<MotorUnitWeights> motor_local_to_contribution_weights` : Coefficients de commande pour convertir la commande de vitesse locale en commande de vitesse de roue.

#### Constructeur
- `Odometry(std::vector<MotorEncoderWheel*> motors, std::vector<MotorPosition*> motors_positions)` : Initialise l'odométrie avec les moteurs et leurs positions.

#### Méthodes Publiques
- `void update()` : Met à jour les encodeurs des moteurs et calcule le mouvement local et absolu.
- `Speed get_speed_local()` : Retourne la vitesse locale.
- `Speed get_speed_absolute()` : Retourne la vitesse absolue.
- `Position get_position_absolute()` : Retourne la position absolue.
- `void set_position_absolute(Position)` : Définit la position absolue.
- `std::vector<mm_per_s> compute_speed_local_to_motors(Speed)` : Calcule la vitesse des moteurs à partir d'une vitesse locale.

#### Méthodes Privées
- `void compute_weights()` : Calcule les poids pour la conversion des mouvements.
- `Position resolve_local_odometry(std::vector<Contribution> contributions)` : Résout l'odométrie locale à partir des contributions des moteurs.

## Fichier `coordinate.hpp`

### Classe `Vector2D`
Représente un vecteur dans un espace 2D.

#### Membres Publics
- `float x, y` : Composantes du vecteur.

#### Constructeurs
- `Vector2D()` : Initialise le vecteur à (0, 0).
- `Vector2D(float x, float y)` : Initialise le vecteur avec les composantes spécifiées.

#### Opérateurs
- `Vector2D operator+(Vector2D const another_coord) const` : Addition de vecteurs.
- `Vector2D operator-(Vector2D const another_coord) const` : Soustraction de vecteurs.
- `Vector2D operator*(const float lambda) const` : Multiplication par un scalaire.
- `Vector2D operator/(const float lambda) const` : Division par un scalaire.
- `Vector2D& operator+=(const Vector2D& other)` : Addition et affectation.

#### Méthodes
- `float angle() const` : Calcule l'angle du vecteur.
- `Vector2D rotate(rad angle) const` : Rotation du vecteur.
- `float get_abs() const` : Calcule la norme du vecteur.
- `Vector2D set_abs(float abs)` : Ajuste la norme du vecteur.
- `Vector2D project_on(Vector2D target_vector) const` : Projection sur un autre vecteur.
- `float scalar_product(Vector2D other_vector) const` : Produit scalaire avec un autre vecteur.
- `static Vector2D unit_vector(rad angle)` : Crée un vecteur unitaire.
- `void print(char str[]) const` : Affiche le vecteur.

---
---

### Classe `Vector2DAndRotation`
Représente un vecteur dans un espace 2D avec une rotation.

#### Membres Publics
- `Vector2D x_y` : Vecteur 2D.
- `float teta` : Angle de rotation.

#### Constructeurs
- `Vector2DAndRotation()` : Initialise à (0, 0, 0).
- `Vector2DAndRotation(float x, float y, float teta)` : Initialise avec les valeurs spécifiées.
- `Vector2DAndRotation(Vector2D x_y, float teta)` : Initialise avec un `Vector2D` et un angle.

#### Opérateurs
- `Vector2DAndRotation operator+(const Vector2DAndRotation another_coord) const` : Addition de vecteurs avec rotation.
- `Vector2DAndRotation operator-(const Vector2DAndRotation another_coord) const` : Soustraction de vecteurs avec rotation.
- `Vector2DAndRotation operator*(const float lambda) const` : Multiplication par un scalaire.
- `Vector2DAndRotation operator/(const float lambda) const` : Division par un scalaire.
- `Vector2DAndRotation& operator+=(const Vector2DAndRotation& other)` : Addition et affectation.

#### Méthodes
- `float get_abs() const` : Calcule la norme du vecteur.
- `Vector2DAndRotation rotate_from_origin(rad angle) const` : Rotation du vecteur par rapport à l'origine.
- `Vector2DAndRotation rotate_locally(rad angle) const` : Rotation locale du vecteur.
- `void print() const` : Affiche le vecteur avec rotation.

## Fichier `user.cpp`
Ce fichier est un équivalent du fichier main dans un code classique. Il contient l'intégration avec le projet `core` et le bus `CAN`.

### variables
 - `std::vector<MotorEncoderWheel*> motors` : Vecteur de moteurs + encodeurs + roues.
 - `std::vector<MotorPosition*> motors_position` : Vecteur contenant les position de chaque `motors`.
 - `Asservisment* current_asserv` : Variable contenant l'asservissement actuellement en cours.
 - `Odometry* odometry` : Variable contenant l'odometry du robot.
 - `RepeatingTimer* repeating_timer` : Variable permettant de gérer le temps sans bloquer le code.

### enum `commands`

Toutes les commandes recevable sur le CAN par la base mobile sont regroupées ici.
```cpp
enum Commands {
    CMD_UNPLUG_MOTORS = 0,
    CMD_STOP = 1,
    CMD_AIM_SPEED_RELATIV = 2,
    CMD_AIM_SPEED_ABSOLUTE = 3,
    CMD_GO_TO_RELATIV = 4,
    CMD_GO_TO_ABSOLUTE = 5,
    CMD_GET_ABSOLUTE_POSITION = 6,
    CMD_SET_ABSOLUTE_POSITION = 7,
    CMD_GET_RELATIV_SPEED = 8,
    CMD_GET_ABSOLUTE_SPEED = 9,
};
```
|Commande|ID|Arguments|Fonctionnement|
|:-:|:-:|:-:|:-:|
|CMD_UNPLUG_MOTORS|0|N/A|"Débranche" logiciellement les moteurs|
|CMD_STOP|1|N/A|Stop le robot le plus vite possible à la position actuelle|
|CMD_AIM_SPEED_RELATIV|2|x,y,$$\theta$$|Fait avancer/tourner le robot à la vitesse souhaitée selon les axes x et y du robot|
|CMD_AIM_SPEED_ABSOLUTE|3|x,y,$$\theta$$|Fait avancer/tourner le robot à la vitesse souhaitée selon les axes x et y du terrain|
|CMD_GO_TO_RELATIV|4|*à compléter*|*à compléter*|
|CMD_GO_TO_ABSOLUTE|5|*à compléter*|*à compléter*|
|CMD_GET_ABSOLUTE_POSITION|6|N/A|Retourne la position du robot sur le terrain|
|CMD_SET_ABSOLUTE_POSITION|7|x,y,$$\theta$$|Régle la position du robot sur le terrain|
|CMD_GET_RELATIV_SPEED|8|N/A|Retourne la vitesse du robot par rapport a lui même|
|CMD_GET_ABSOLUTE_SPEED|9|N/A|Retourne la vitesse du robot par rapport au terrain|

#### Méthodes
 - `void asservisment_set_safe(Asservissment* new_asserv)` : Méthodes permettant de changer l'asservissement actuel du robot sans fuites de mémoire.
 - `void Talos_initialisation()` : Méthode équivalente à `void setup()` en arduino ; s'exécute une seule fois au début du programme.
 - `void Talos_loop()` : Méthode équivalente à `void loop()` en arduino ; s'exécute en boucle après l'initialisation tant qu'il n'y à pas d'erreur.
 - `void Talos_onError()` : Boucle qui s'exécute en cas d'erreur dans **core** ou dans la **base mobile**.
 - `void Core_priorityLoop()` : Boucle qui s'exécute quel que soit l'état de **core** ou de la **base mobile**.
 - `void Hermes_onMessage(Hermes_t* hermesInstance, uint8_t sender,uint16_t command, uint32_t isResponse)` : Méthode qui gère la réception des commandes sur le bus **CAN** et qui fais le lien avec les méthodes de la **base mobile**.