# TEST setup for the omni 2024

[[_TOC_]]

## Schéma d'un banchemement test d'un moteur

Initialisation de ce moteur dans TALOS_init avec la ligne suivante :

> Rayon de la roue du moteur = 58/2 <br>
> Nombre de pas d'encodeur par tour = ((64 / 2) / 2) * 18.5

/!\  Ces deux points sont susceptible de changer en fonction de chaque ;taille de roue; & ;implémentation dans le code de la lecture des encodeurs & nombre d'encodeur par révolution;

```
motor = new Motor(58 / 2, 64 / 2 / 2 * 18.5, PA1, PA3, PA8, PB0, PB7);
```

| ![branchement_stm.jpg](/images_md/Branchement_stm_test.jpg) | 
|:--:| 
| *Figure 1 : Branchement de la stm32 L432KC avec le moteur et le driver* |

Les drivers utilisés sont des L298N.

| ![branchement_driver.jpg](/images_md/Branchement_driver_test.jpg.jpg) | 
|:--:| 
| *Figure 2 : Branchement driver avec l'alimentation et le moteur* |


## Schéma d'un branchement test à trois moteur

