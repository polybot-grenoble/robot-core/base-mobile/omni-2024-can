# Organisation du code

Présentation de la direction et de l'agencement des éléments présent dans le code du module base omni-directionnel.

[[_TOC_]]

## Code structure

```mermaid
classDiagram
    CAN -- AsservismentAbstract
    AsservismentAbstract ..> AsservExample
    AsservExample ..> Odometry
    Odometry ..> MotorEncoderWheel
    AsservExample ..> MotorEncoderWheel
    MotorEncoder <|-- Motor
    MotorEncoder <|-- Encoder
    MotorEncoder ..> PID
    MotorEncoderWheel <|-- MotorEncoder

class CAN{
        
}

namespace Omni {
    class AsservismentAbstract{
        +constructor()*
        +resume()*
        +update()*

        #unplug_motors()$
        #command_motors_for_local_speed()$
        #update_motors_speed()$
    }

    class AsservExample{
        +constructor()
        +resume()
        +update()
    }

    class Odometry{
        #position_absolute
        #speed_local
        #motors[]
        #motors_positions[]

        #motor_contribution_to_local_weights[]
        #motor_local_to_contribution_weights[]

        +update()
        +get_speed_local()
        +get_speed_absolute()
        +get_position_absolute()
        +set_position_absolute()
        +compute_speed_local_to_motors()
        +speed_local_to_absolute()
        +speed_absolute_to_local()
        +print_weights()

        -compute_weights()
        -resolve_local_odometry()
    }

    class PID{
        -KP
        -KI
        -KD
        -error_previous
        -error_integral
        -max_integral_absolute_weight

        +set_parameters()
        +set_target()
        +reset_errors()
        +execute_pid_control()
    }

    class Motor{
        -pins

        +set_motor_power()
        +unplug()
    }
    class Encoder{
        -pins
        #encodeur_step_per_revolution
        #nb_step_encodeur
        #enc_step_last_period
        #time_previous
        #time_delta

        #measure_computed
        #rpm_last_period_filtered
        #rot_last_period
        #rpm_last_period_previous_unfiltered

        -handle_enc_interrupt_a()
        -handle_enc_interrupt_b()

        +pop_encoder_count_period()
        +get_rot_last_period()
        +get_rpm()
        -compute_movement()
    }
    class MotorEncoder{
        #pid_rpm
        +set_rpm()
        +update_rpm()
        +set_pid_parameters()
        +unplug()
    }
    class MotorEncoderWheel{
        #wheel_perimeter

        +get_traveled_distance_last_period()
        +get_speed_last_period()
        +set_speed()
        +update_speed()
        +unplug()
    }
}
```
Legend following mermaid standards :
```mermaid
classDiagram

classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)

class ClassContent {
    +public_attribute
    -private_attribute
    #protected_attribute
    static_attribute$

    abstract_method()*
    static_method()$
}
```

## Execution order

The following pseudo-code aim at explaining the execution order for whole Talos' functions.

> `Talos_initialisation() {`
>
> - Motors instanciation
>   - pinMode
>   - attachInterrupt
> - Odometry instanciation
>   - Compute weights
> - Set asservissment to unplug
>
> `}`

<!--  -->

> `Talos_loop() {`
>
> - Odometry update
>   - Motors update
>   - Motors get contributions
>   - Resolve contributions to local movement
>   - Propagate local movement to global
> - Current asservissment update
>
> `}`

<!--  -->

> `Talos_onError() {`
>
> - Set asservissment to unplug
>
> `}`

## Development roadmap

_🚧 work in progress, ✏️ written but not tested yet, ✅ done_

* ✅ Motorisation (tested)
  * Encoder
    * Interrupt
    * Compute rotation
    * Compute rotation speed
  * Motor
    * Set raw power
  * MotorEncoder
    * PID rpm (fine tuned)
  * MotorEncoderWheel
    * set mm/s
* ✅ Odometry
  * Compute motor contributions weights
  * Compute motor projection weights
  * Update encoders
  * Compute contributions
  * 🚧 interpolate movement
  * Propagate movement to absolute position
* 🚧 Asservissments
  * ✅ Unplug motors
  * ✅ Zero-speed
  * ✏️ Zero-speed-response
  * ✅ Relative speed
    * 🚧 Obstacle avoidance
  * ✅ Absolute speed
    * 🚧 Obstacle avoidance
  * Relative position
  * 🚧 Absolute position
    * ✅ CAN command
    * ✏️ Position_absolute_goto::update (bad approach speed)
* ✏️ CAN command handling

## Motor

The `Motor` class aim to be a light interface to a real continuous current motor (thgrough a driver with two enable and one pwm). It only provide a method to `set_motor_power(power)` in [-255; 255] as well as `unplug()` a short for `set_motor_power(0)`.

Behind the scene the constructor set the needed pin's modes.

## Encoder

The `Encoder` class aim to handle `A` and `B` signals to compute rotation and rotation's speed. To do so we descretize time in periods, a period is splitted from the next by a `pop_encoder_count_period()` call. To have multiple encoders working simultaneously with perdiods as close from one to another, `pop_encoder_count_period()` should be called as fast as possible for all of them at once. The math behind encoder's steps to rotation is done lazily when asked for.

## MotorEncoder

The `MotorEncoder` class enable a closed loop asservisment.

```mermaid
---
title: MotorEncoder
---
flowchart LR

Command -- Desired rotation --> Diff
Motor -- rotation --> Shaft --> Output
Shaft --> Encoder -- Measured rotation --> Diff -- error --> PID -- power --> Motor
```

## PID

The `PID` class is self-explanatory.

## MotorEncoderWheel

The `MotorEncoderWheel` is a wrapper around `MotorEncoder` (heritance) that provide rotation to distance and distance to rotation conversion from wheel diameter.

## Odometry

The `Odometry` class is aware of each motors' potition in the robot's origin. Enabling it to compute the robot's movement (speed and position) from each motors' contribution. By summing up each motors movement it keep track of the robot's position in table's origin. It also provide a way to compute motors' individual speed to achieve a robot's speed.

## AsservismentAbstract

`AsservismentAbstract` is a base class for ExampleAsservisment. It is used to store an any asservisment, and give an interface with `AsservismentAbstract::resume` and `AsservismentAbstract::update` methods.

`AsservismentAbstract` class also provide protected static methods for child class to use: `AsservismentAbstract::unplug_motors`, `AsservismentAbstract::command_motors_for_local_speed` and `AsservismentAbstract::update_motors_speed`.
