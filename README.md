# Utilisation de la base mobile holonome

Code Arduino pour la base mobile holonome à 3 roues omnidirectionnelles, pour la carte NUCLEO L432KC.

## Commands

| id | action | parameters | response |
|----|--------|------------|----------|
|0|Unplug motors| | |
|1|Stop, aim null speed| | |
|2|Aim speed relative to **robot's origin**| x', y' and θ' (floats) | |
|3|Aim speed relative to **table's origin**| x', y' and θ' (floats) | |
|4|Go to coordinate relative to **robot's origin**| x, y and θ (floats) | |
|5|Go to coordinate relative to **table's origin**| x, y and θ (floats) | |
|6|Set rebot position relative to **table's origin**| x, y and θ (floats) | |
|7|Get rebot position relative to **table's origin**| | x, y and θ (floats) |
|8|Get rebot speed relative to **robot's origin**| | x', y' and θ' (floats) |
|9|Get rebot speed relative to **table's origin**| | x', y' and θ' (floats) |
|10|Resume previous given asservisment (pop a history of limited length) if it isn't stopped by obstacle| | success (bool (uint8)) |
|11| Persistent stop for obstacles | | |
|12| Is the mobile base currently stopped by an obstacle | | Is stopped (bool (uint8)) |
|13| Resume previous asservisment before obstacle | | |
|14|Stop, aim null speed with response| none | none |

## Wiring
Refer to [this file](Fonctionnement_moteur.md)

## Function implementation
Refer to [this file](organisation_du_code.md)

## Platform.io
[Can not find Python Interpreter](https://github.com/platformio/platformio-core-installer/issues/1774)
