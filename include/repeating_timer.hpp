#ifndef REPEATING_TIMER
#define REPEATING_TIMER

class RepeatingTimer {
   private:
    const unsigned long period_us;
    unsigned long time_previous_us;

   public:
    RepeatingTimer(const unsigned long period_us);
    bool is_time_up();
};

#endif
