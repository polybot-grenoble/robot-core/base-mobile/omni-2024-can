#ifndef ASSERVISMENT_HISTORY
#define ASSERVISMENT_HISTORY

#include "base_mobile/asservisment.hpp"

AsservismentAbstract* asservisment_current_get();

// Take the given asservisment ownership.
void asservisment_new_set(AsservismentAbstract* asservisment_new, const bool even_with_obstacle=false);

bool asservisment_previous_resume(const bool even_with_obstacle=false);

#endif
