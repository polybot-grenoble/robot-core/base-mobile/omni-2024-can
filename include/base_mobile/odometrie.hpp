#ifndef ODOMETRIE
#define ODOMETRIE

#include <vector>

#include "base_mobile/coordinates.hpp"
#include "base_mobile/debug.hpp"
#include "base_mobile/motor.cpp"
#include "core/unit.hpp"

typedef Position MotorPosition;  // maybe we should call these WheelPosition
typedef Vector2DAndRotation
    MotorUnitWeights;  // maybe we should call these WheelWeights

/**
 * @brief Position local d'un moteur par rapport au centre de la base mobile
 * @protected local_position Une
 * @public .MotorPosition()
 */

typedef struct {
    mm travelled_distance;
    mm_per_s speed;
} Contribution;

class Odometry {
   protected:
    Position position_absolute;
    Speed speed_local;
    const std::vector<MotorEncoderWheel*> motors;
    const std::vector<MotorPosition*> motors_positions;

    /**
     * Odometry coefficients
     * Needed to convert wheel movement into weighted local movement
     * Units for X & Y : mm / mm
     * Units for teta : rad / (mm.mm)
    */
    std::vector<MotorUnitWeights> motor_contribution_to_local_weights;

    /**
     * Command coefficients
     * Needed to convert local speed command to wheel speed command
     * Units for X & Y : (mm/s) / (mm/s)
     * Units for teta : (mm.mm/s) / (rad/s)
    */
    std::vector<MotorUnitWeights> motor_local_to_contribution_weights;

   public:
    Odometry(std::vector<MotorEncoderWheel*> motors,
             std::vector<MotorPosition*> motors_positions);

    /**
     * 1. Update motors encoders
     * 2. Get each motors contribution
     * 3. resolve to local movement (and save speed)
     * 4. Propagate movement to absolute
     */
    void update();

    Speed get_speed_local();
    Speed get_speed_absolute();

    Position get_position_absolute();
    void set_position_absolute(Position);

    std::vector<mm_per_s> compute_speed_local_to_motors(Speed);

    Speed speed_local_to_absolute(Speed speed_local);
    Speed speed_absolute_to_local(Speed speed_absolute);

    void print_weights() const;

   private:
    void compute_weights();
    Position resolve_local_odometry(std::vector<Contribution> contributions);
};

#endif
