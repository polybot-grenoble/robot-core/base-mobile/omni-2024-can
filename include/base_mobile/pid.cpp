#include <Arduino.h>

#include "base_mobile/debug.hpp"
#include "core/unit.hpp"

class PID {
   private:
    float KP, KI, KD;
    float error_previous, error_integral;
    float max_integral_absolute_weight;

   protected:
    float target;

   public:
   /**
    * @brief Construct a new PID object
    * Default values for a Pololu 12V motor's rpm asservissment.
    */
    PID()
        : KP(10),
          KI(20),
          KD(0.01),
          error_previous(0.0),
          error_integral(0.0),
          max_integral_absolute_weight(255) {}
    PID(float KP, float KI, float KD)
        : KP(KP),
          KI(KI),
          KD(KD),
          error_previous(0.0),
          error_integral(0.0),
          max_integral_absolute_weight(255) {}
    PID(float KP, float KI, float KD, float max_integral_absolute_weight)
        : KP(KP),
          KI(KI),
          KD(KD),
          error_previous(0.0),
          error_integral(0.0),
          max_integral_absolute_weight(max_integral_absolute_weight) {}

    /**
     * @brief Setup of the paramaters for the Pid
     * @param
     */
    void set_parameters(float KP, float KI, float KD,
                        float max_integral_absolute_weight) {
        this->KP = KP;
        this->KI = KI;
        this->KD = KD;
        this->max_integral_absolute_weight = max_integral_absolute_weight;
    }

    /**
     * @note À finir et pensez à segmenter les updates du moteur.
     */
    void set_target(float target) { this->target = target; }

    void reset_errors(void){
        error_previous = 0.0;
        error_integral = 0.0;
    }

    float execute_pid_control(float value_current, micro_second time_delta) {
        // Calcul de l'erreur entre la vitesse desiré et la vitesse actuelle

        const float error_now = target - value_current;

        // derivative
        const float error_derivative =
            (error_now - error_previous) * 1e6f / time_delta;
        // float dedt = (e-eprev)/(deltaT);

        // integral
        this->error_integral += error_now * time_delta / 1e6f;

        // anti windup
        if (error_integral > max_integral_absolute_weight / KI)
            error_integral = max_integral_absolute_weight / KI;
        else if (error_integral < -max_integral_absolute_weight / KI)
            error_integral = -max_integral_absolute_weight / KI;

        // control signal
        float control_signal =
            KP * error_now + KI * error_integral + KD * error_derivative;

        // store previous error
        error_previous = error_now;

        if (LOG_PID) {
            Serial.print(";pid=");
            Serial.print(control_signal);
            Serial.print("=(");
            Serial.print(error_now);
            Serial.print("=e*");
            Serial.print(KP);
            Serial.print(")+(");
            Serial.print(error_integral);
            Serial.print("=ei*");
            Serial.print(KI);
            Serial.print(")+(");
            Serial.print(error_derivative);
            Serial.print("=ed*");
            Serial.print(KD);
            Serial.print(")");
        }

        return control_signal;
    }
};
