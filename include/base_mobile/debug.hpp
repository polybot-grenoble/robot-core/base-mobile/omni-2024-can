#ifndef __DEBUG__
#define __DEBUG__

#define DEBUG_ON true

#define DEBUG_ALL false

#define LOG_MOT (DEBUG_ON & false) | DEBUG_ALL

#define LOG_PID (DEBUG_ON & false) | DEBUG_ALL

#define LOG_USE (DEBUG_ON & false) | DEBUG_ALL

#define LOG_ODO (DEBUG_ON & false) | DEBUG_ALL

#define LOG_ASS (DEBUG_ON & false) | DEBUG_ALL

#endif
