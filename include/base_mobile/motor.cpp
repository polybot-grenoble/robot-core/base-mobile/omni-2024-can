#ifndef __CLASS_MOTOR__
#define __CLASS_MOTOR__

#include <Arduino.h>
#include <stdint.h>

#include <algorithm>  // min and abs

#include "base_mobile/debug.hpp"
#include "base_mobile/pid.cpp"
#include "core/unit.hpp"

// Set the printing for debug values
#define debug 0

class Encoder {
   private:
    const pin pin_encoderA, pin_encoderB;

   protected:
    // Géométrie
    const float enc_step_per_revolution;

    // Instantané
    int nb_step_encodeur;

    // Dernière mesure
    int enc_step_last_period;
    micro_second time_previous;
    micro_second time_delta;

    // Dernier calcul
    bool measure_computed;
    rpm rpm_last_period_filtered;
    // int rot_last_period;
    rot rot_last_period;

    // filtrage de la vitesse
    rpm rpm_last_period_previous_unfiltered;

   public:
    Encoder(float enc_step_per_revolution, pin pin_encoderA, pin pin_encoderB)
        : enc_step_per_revolution(enc_step_per_revolution / 2),
          pin_encoderA(pin_encoderA),
          pin_encoderB(pin_encoderB),
          nb_step_encodeur(0),
          rpm_last_period_previous_unfiltered(0),
          rpm_last_period_filtered(0),
          time_previous(micros()) {

        pinMode(pin_encoderA, INPUT);
        pinMode(pin_encoderB, INPUT);

        attachInterrupt(pin_encoderA, [&]() { this->handle_enc_interrupt_a(); }, CHANGE);
        // attachInterrupt(pin_encoderB, [&]() { this->handle_enc_interrupt_b(); }, CHANGE);
    }

    void handle_enc_interrupt_a() {
        const int a = digitalRead(this->pin_encoderA);
        const int b = digitalRead(this->pin_encoderB);
        if (a == b)
            this->nb_step_encodeur++;
        else
            this->nb_step_encodeur--;
    }
    void handle_enc_interrupt_b() {
        const int a = digitalRead(this->pin_encoderA);
        const int b = digitalRead(this->pin_encoderB);
        if (a != b)
            this->nb_step_encodeur++;
        else
            this->nb_step_encodeur--;
    }

    void pop_encoder_count_period() {
        // Dans la variable nb_step_encodeur_now il y a le nombre de pas
        // d'encodeur depuis la dernière pop_encoder_count_period
        this->enc_step_last_period = this->nb_step_encodeur;
        this->nb_step_encodeur -= enc_step_last_period;

        micro_second time_now = micros();
        time_delta = time_now - time_previous;
        time_previous = time_now;

        measure_computed = false;
    }

    rot get_rot_last_period() {
        this->compute_movement();
        return this->rot_last_period;
    }

    /**
     * @brief RPM (rotation per minute) segment
     */
    rpm get_rpm() {
        this->compute_movement();
        return this->rpm_last_period_filtered;
    }

   private:
    /**
     * Motor màj
     */
    void compute_movement() {
        if (!measure_computed) {
            // calcul de l'angle parcouru
            rot_last_period = (enc_step_last_period) / ( enc_step_per_revolution);
            // Calcul rotation par minute
            rpm rpm_last_period_unfiltered = enc_step_last_period * 60 * 1e6 /
                                             enc_step_per_revolution /
                                             time_delta;
            // la vitesse calculé est sensible au hautes fréquences,
            // ce filtrage numérique coupe les fréquences >25 Hz
            // v_filtre[now] = 0.854*v_filtre[previous] + 0.0728*v[now] +
            // 0.0728*v[previous]
            this->rpm_last_period_filtered =
                0.8544 * rpm_last_period_filtered +
                0.0728 * rpm_last_period_unfiltered +
                0.0728 * rpm_last_period_previous_unfiltered;
            this->rpm_last_period_previous_unfiltered =
                rpm_last_period_unfiltered;

            measure_computed = true;
        }
    }
};

class Motor {
   private:
    const pin pin_pwm, pin_direction_positive, pin_direction_negative;

   public:
    Motor(const pin pin_pwm, const pin pin_direction_positive,
          const pin pin_direction_negative)
        : pin_pwm(pin_pwm),
          pin_direction_positive(pin_direction_positive),
          pin_direction_negative(pin_direction_negative) {
        pinMode(pin_pwm, OUTPUT);
        pinMode(pin_direction_positive, OUTPUT);
        pinMode(pin_direction_negative, OUTPUT);
    }

    // The - or + in power choice is related to the omni-base rotation (sens
    // trig roue = -)
    /**
     * @param power : -255 to 255 inclusive power
     * @brief Motor control thought power management
     */
    void set_motor_power(int power) {
        const uint8_t power_abs = std::min(std::abs(power), 255);

        analogWrite(this->pin_pwm, power_abs);

        digitalWrite(this->pin_direction_positive, power > 0 ? HIGH : LOW);
        digitalWrite(this->pin_direction_negative, power < 0 ? HIGH : LOW);
    }

    void unplug() { this->set_motor_power(0);}
};

class MotorEncoder : public Encoder, public Motor {
   protected:
    // RPM control
    PID pid_motor_rpm;

   public:
    MotorEncoder(const pin pin_encoderA, const pin pin_encoderB,
                 const pin pin_pwm, const pin pin_direction_positive,
                 const pin pin_direction_negative,
                 const float enc_step_per_revolution, const float KP,
                 const float KI, const float KD,
                 const float max_integral_absolute_weight)
        : Encoder(enc_step_per_revolution, pin_encoderA, pin_encoderB),
          Motor(pin_pwm, pin_direction_positive, pin_direction_negative),
          pid_motor_rpm(PID(KP, KI, KD, max_integral_absolute_weight)) {}

    void set_rpm(const rpm rpm_desired) {
        this->pid_motor_rpm.set_target(rpm_desired);
        update_rpm();
    }
    void update_rpm() {
        set_motor_power(
            pid_motor_rpm.execute_pid_control(get_rpm(), time_delta));
    }

    void set_pid_parameters(const float KP, const float KI, const float KD,
                            const float max_integral_absolute_weight) {
        pid_motor_rpm.set_parameters(KP, KI, KD, max_integral_absolute_weight);
    }

    void unplug() { 
        this->Motor::unplug();
        pid_motor_rpm.reset_errors();
    }
};

class MotorEncoderWheel : public MotorEncoder {
   protected:
    const mm WHEEL_PERIMETER;

   public:
    MotorEncoderWheel(pin pin_encoderA, pin pin_encoderB, pin pin_pwm,
                      pin pin_direction_positive, pin pin_direction_negative,
                      float enc_step_per_revolution, mm wheel_diameter,
                      const float KP, const float KI, const float KD,
                      const float max_integral_absolute_weight)
        : MotorEncoder(pin_encoderA, pin_encoderB, pin_pwm,
                       pin_direction_positive, pin_direction_negative,
                       enc_step_per_revolution, KP, KI, KD,
                       max_integral_absolute_weight),
          WHEEL_PERIMETER(PI * wheel_diameter) {}

    mm get_traveled_distance_last_period() {
        return this->get_rot_last_period() * WHEEL_PERIMETER;
    }

    mm_per_s get_speed_last_period() {
        return (this->get_rpm() * WHEEL_PERIMETER) / 60;
    }

    void set_speed(mm_per_s desired_speed) {
        set_rpm((desired_speed * 60) / WHEEL_PERIMETER);
    }

    void update_speed() { update_rpm(); }

    void unplug() { this->MotorEncoder::unplug(); }
};

#endif
