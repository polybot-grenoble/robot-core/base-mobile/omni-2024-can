#ifndef ASSERVISMENT
#define ASSERVISMENT

#include <algorithm>
#include <vector>

#include "base_mobile/coordinates.hpp"
#include "base_mobile/debug.hpp"
#include "base_mobile/motor.cpp"
#include "base_mobile/odometrie.hpp"
#include "core/core.hpp"

// Speed and acceleration
const mm_per_s SPEED_TRANS_MAX = 1000;
const mm_per_s_per_s ACC_TRANS_MAX = 500;
const rad_per_s SPEED_ANG_MAX = PI;
const rad_per_s_per_s ACC_ANG_MAX = PI / 2;

// Stop thresholds
const mm DISTANCE_TRANS_STOP_THRESHOLD = 5;
const rad DISTANCE_ANG_STOP_THRESHOLD = .2;
const mm_per_s SPEED_TRANS_STOP_THRESHOLD = 100;
const rad_per_s SPEED_ANG_STOP_THRESHOLD = .4;

class AsservismentAbstract {
    /**
     * To understand virtual choices made here, read :
     * https://www.stroustrup.com/bs_faq2.html#virtual-ctor
     */
   public:
    // Constructor signature example :
    // AsservismentAbstract(HermesBuffer* hermes_message, Odometry &odometry,
    // std::vector<MotorEncoderWheel&> Motors);

    // resume is intended to be called after a resume command, motors may have
    // got any new order.
    virtual void resume() {}
    virtual void update() {}

   protected:
    static void unplug_motors(std::vector<MotorEncoderWheel *> motors) {
        std::for_each(motors.begin(), motors.end(),
                      [](MotorEncoderWheel *motor) { motor->unplug(); });
    }
    static void command_motors_for_local_speed(
        std::vector<MotorEncoderWheel *> motors, Odometry *odometry,
        Speed local_speed) {
        // Serial.print("command_motors_for_local_speed");
        // local_speed.print();
        // Serial.print("→[");
        // Compute each motor contribution for the given local_speed.
        std::vector<mm_per_s> motors_speed =
            odometry->compute_speed_local_to_motors(local_speed);

        // Apply command to each motors.
        for (size_t motor_index = 0; motor_index < motors.size();
             motor_index++) {
            mm_per_s motor_speed = motors_speed[motor_index];
            // if (motor_index)
            //     Serial.print(";");
            // Serial.print(motor_speed);
            MotorEncoderWheel *motor = motors[motor_index];

            motor->set_speed(motor_speed);
        }
        // Serial.print("]");
        // Serial.println("");
    }
    static void update_motors_speed(std::vector<MotorEncoderWheel *> motors) {
        std::for_each(motors.begin(), motors.end(),
                      [](MotorEncoderWheel *motor) { motor->update_speed(); });
    }
};

class AsservUnplug : public AsservismentAbstract {
    std::vector<MotorEncoderWheel *> motors;

   public:
    AsservUnplug(std::vector<MotorEncoderWheel *> motors) : motors(motors) {
        resume();
    }
    void resume() { unplug_motors(motors); }
};

class AsservSpeedZero : public AsservismentAbstract {
    std::vector<MotorEncoderWheel *> motors;

   public:
    AsservSpeedZero(std::vector<MotorEncoderWheel *> motors) : motors(motors) {
        resume();
    }
    void resume() {
        std::for_each(motors.begin(), motors.end(),
                      [](MotorEncoderWheel *motor) { motor->set_speed(0); });
    }
    void update() { update_motors_speed(motors); }
};

class AsservSpeedZeroResponse : public AsservSpeedZero {
    Odometry *const odometry;
    Hermes_t *const hermesInstance;
    const uint8_t command_sender_id;
    const uint16_t command_id;
    bool response_sent;
    const mm_per_s max_translation_speed = 2;
    const rad_per_s max_angular_speed = .15;

   public:
    AsservSpeedZeroResponse(std::vector<MotorEncoderWheel *> motors,
                            Odometry *odometry, Hermes_t *hermesInstance,
                            uint8_t command_sender_id, uint16_t command_id)
        : AsservSpeedZero(motors),
          odometry(odometry),
          hermesInstance(hermesInstance),
          command_sender_id(command_sender_id),
          command_id(command_id),
          response_sent(false) {}
    void update() {
        AsservSpeedZero::update();
        if (!response_sent && is_zero_speed_reached()) {
            send_response();
        }
    }

   protected:
    bool is_zero_speed_reached() {
        Speed speed_local = odometry->get_speed_local();
        return speed_local.get_abs() <= max_translation_speed &&
               speed_local.teta <= max_angular_speed;
    }
    void send_response() {
        if (Hermes_send(hermesInstance, command_sender_id, command_id, true,
                        "") == HERMES_OK)
            response_sent = true;
    }
};

class AsservSpeedZeroObstacle : public AsservSpeedZero {
    std::vector<MotorEncoderWheel *> motors;

   public:
    AsservSpeedZeroObstacle(std::vector<MotorEncoderWheel *> motors) : AsservSpeedZero(motors) {}
};

class AsservSpeedRelative : public AsservismentAbstract {
   protected:
    const std::vector<MotorEncoderWheel *> motors;
    Odometry *const odometry;
    const Speed speed_local;

   public:
    AsservSpeedRelative(std::vector<MotorEncoderWheel *> motors,
                        Odometry *odometry, Speed speed_local)
        : motors(motors), odometry(odometry), speed_local(speed_local) {
        resume();
    }
    void resume() {
        command_motors_for_local_speed(motors, odometry, speed_local);
    }
    void update() {
        update_motors_speed(motors);
        // Serial.print(";command_loc=");
        // speed_local.print();
    }
};

class AsservSpeedAbsolute : public AsservismentAbstract {
   protected:
    const std::vector<MotorEncoderWheel *> motors;
    Odometry *const odometry;
    const Speed absolute_speed;

   public:
    AsservSpeedAbsolute(std::vector<MotorEncoderWheel *> motors,
                        Odometry *odometry, Speed absolute_speed)
        : motors(motors), odometry(odometry), absolute_speed(absolute_speed) {
        update();
    }
    void update() {
        // Serial.print(";command:abs");
        // absolute_speed.print();
        // Transpose speed to local origin.
        const Speed local_speed =
            odometry->speed_absolute_to_local(absolute_speed);
        // Serial.print("→loc");
        // local_speed.print();

        command_motors_for_local_speed(motors, odometry, local_speed);
    }
};

class AsservPositionAbsolute : public AsservismentAbstract {
   protected:
    const std::vector<MotorEncoderWheel *> motors;
    Odometry *const odometry;
    const Position command_absolute_position;

    micro_second time_previous, time_delta;
    Speed speed_command_last_absolute;

    const uint8_t approch_function = 0;
    bool position_reached;
    Hermes_t *const hermes_instance;
    const uint8_t sender_id;
    const uint16_t command_id;

   public:
    AsservPositionAbsolute(std::vector<MotorEncoderWheel *> motors,
                           Odometry *odometry,
                           Position command_absolute_position,
                           Hermes_t *hermes_instance, uint8_t sender_id,
                           uint16_t command_id)
        : motors(motors),
          odometry(odometry),
          command_absolute_position(command_absolute_position),
          position_reached(false),
          hermes_instance(hermes_instance),
          sender_id(sender_id),
          command_id(command_id) {
        resume();
    };
    void resume() {
        Core_log("Asserv position resume");
        time_previous = micros();
        speed_command_last_absolute = odometry->get_speed_absolute();
        update();
    }

    void update() {
        const Distance &distance_left = command();
        const Speed &speed_current = odometry->get_speed_absolute();

        if (!position_reached &&
            (distance_left.get_abs() <= DISTANCE_TRANS_STOP_THRESHOLD &&
             abs(distance_left.teta) <= DISTANCE_ANG_STOP_THRESHOLD &&
             speed_current.get_abs() <= SPEED_TRANS_STOP_THRESHOLD &&
             abs(speed_current.teta) <= SPEED_ANG_STOP_THRESHOLD)) {
            Core_log("Arrived");
            position_reached |= hermes_instance == nullptr || Hermes_send(hermes_instance, sender_id,
                                                                          command_id, true, "") == HERMES_OK;
        }
    };

   protected:
    Distance command() {
        // 0. Current robot speed and distance left.

        const Speed &speed_current = speed_command_last_absolute;
        const bool IS_SPEED_TRANS_NULL = speed_current.get_abs() <= SPEED_TRANS_STOP_THRESHOLD;
        const bool IS_SPEED_ANG_NULL = abs(speed_current.teta) <= SPEED_ANG_STOP_THRESHOLD;

        Distance distance_left =
            command_absolute_position - odometry->get_position_absolute();
        distance_left.teta =
            frame_angle(distance_left.teta);  // Modulo between -PI and +PI.

        Serial.print(";Left=");
        distance_left.print();

        // 1. Compute desired speed to reach the target at max speed or arrival
        // speed.

        Speed speed_desired = [&]() {
            // // Arrival speed (from https://www.nagwa.com/fr/explainers/903126847604/)
            // const mm_per_s speed_arrival_max = sqrt(2 * ACC_TRANS_MAX * distance_left.x_y.get_abs());
            // const rad_per_s angular_speed_arrival_max = sqrt(2 * ACC_ANG_MAX * abs(distance_left.teta));

            // // desired speed
            // mm_per_s trans_speed_desired_min = min(SPEED_TRANS_MAX, speed_arrival_max);
            // rad_per_s angular_speed_desired_min = min(SPEED_ANG_MAX, angular_speed_arrival_max);

            // Desired speed with sigmoid arrival
            // auto smooth_arrival = [](float left, float cruise, float min_left_threshold, float max_deriv, float min_value) -> float {
            //     if (left <= min_left_threshold)
            //         return 0;
            //     const float coef_pente = max_deriv * 4 / cruise;
            //     const float offset = log(cruise / min_value - 1);
            //     return cruise / (1 + exp(-coef_pente * left + offset));
            // };
            // const mm_per_s trans_speed_desired_min = smooth_arrival(distance_left.x_y.get_abs(), SPEED_TRANS_MAX, DISTANCE_TRANS_STOP_THRESHOLD / 3, 1, SPEED_TRANS_STOP_THRESHOLD / 2 / 2);
            // const rad_per_s angular_speed_desired_min = smooth_arrival(abs(distance_left.teta), SPEED_ANG_MAX, DISTANCE_ANG_STOP_THRESHOLD / 3, 6e-4, SPEED_ANG_STOP_THRESHOLD);

            // // Desired speed with sinusoid arrival
            // auto desired_sin_arrival = [](float left, float cruise, float left_tolerance, float arrival_width) -> float {
            //     if (left <= left_tolerance)
            //         return 0;
            //     if (left >= left_tolerance + arrival_width)
            //         return cruise;
            //     return cruise/2 * (sin((left - left_tolerance) * PI / arrival_width)+1);
            // };
            // Serial.print(";sin:");
            // Serial.print(desired_sin_arrival(distance_left.x_y.get_abs(), 1000, DISTANCE_TRANS_STOP_THRESHOLD, 250));
            // const mm_per_s trans_speed_desired_min = desired_sin_arrival(distance_left.x_y.get_abs(), SPEED_TRANS_MAX, DISTANCE_TRANS_STOP_THRESHOLD, 250);
            // const rad_per_s angular_speed_desired_min = desired_sin_arrival(abs(distance_left.teta), SPEED_ANG_MAX, DISTANCE_ANG_STOP_THRESHOLD / 2, PI / 6);

            // Desired speed with sinusoid arrival
            auto sin_step_transition = [](float x, float begin_x, float end_x, float begin_y, float end_y) -> float {
                if (x <= begin_x)
                    return begin_y;
                if (x >= end_x)
                    return end_y;
                return (end_y - begin_y) / 2 * (-cos(((x - begin_x) / (end_x - begin_x)) * PI) + 1) + begin_y;
            };
            Serial.print(";sin:");
            Serial.print(sin_step_transition(distance_left.x_y.get_abs(), DISTANCE_TRANS_STOP_THRESHOLD, 1200, 0, SPEED_TRANS_MAX));
            const mm_per_s trans_speed_desired_min = sin_step_transition(distance_left.x_y.get_abs(), DISTANCE_TRANS_STOP_THRESHOLD, 1200, 0, SPEED_TRANS_MAX);
            const rad_per_s angular_speed_desired_min = sin_step_transition(abs(distance_left.teta), DISTANCE_ANG_STOP_THRESHOLD, PI / 4, 0, PI / 6);

            // // Desired speed with linear arrival
            // const mm_per_s trans_speed_desired_min = std::clamp((mm_per_s) distance_left.x_y.get_abs(), (mm_per_s) 0, SPEED_TRANS_MAX);
            // const rad_per_s angular_speed_desired_min = std::clamp((rad_per_s) abs(distance_left.teta), (rad_per_s) 0, SPEED_ANG_MAX);

            return Speed(
                distance_left.x_y.set_abs(trans_speed_desired_min),
                set_abs_float(distance_left.teta, angular_speed_desired_min));
        }();
        Serial.print(";Desired=");
        speed_desired.print();

        // 2. Compute command

        micro_second time_now = micros();
        time_delta = time_now - time_previous;
        time_previous = time_now;

        const Speed command_speed(
            [&]() -> Vector2D {
                Vector2D speed_current_considered_2D = speed_current.x_y;
                // if (IS_SPEED_TRANS_NULL && speed_desired.get_abs() > SPEED_TRANS_STOP_THRESHOLD) {  // Robot is still and starting
                //     speed_current_considered_2D = speed_desired.x_y.cap_abs(SPEED_TRANS_STOP_THRESHOLD);
                //     // Serial.print(";trans=dep");
                // }

                Vector2D steering_2D = speed_desired.x_y - speed_current_considered_2D;
                mm_per_s speed_delta_max = ACC_TRANS_MAX * time_delta / 1e6f;
                // Cap to max acceleration
                steering_2D = steering_2D.cap_abs(speed_delta_max);
                return speed_current_considered_2D + steering_2D;
            }(),
            [&]() -> rad_per_s {
                return 0;
                rad_per_s speed_ang_current_considered = speed_current.teta;
                // if (IS_SPEED_TRANS_NULL && abs(speed_desired.teta) > SPEED_ANG_STOP_THRESHOLD) {  // Robot is still and starting
                //     speed_ang_current_considered = std::clamp(speed_desired.teta, -SPEED_ANG_STOP_THRESHOLD, SPEED_ANG_STOP_THRESHOLD);
                //     // Serial.print(";ang=dep");
                // }

                rad_per_s steering_ang = speed_desired.teta - speed_ang_current_considered;
                rad_per_s angular_speed_delta_max = ACC_ANG_MAX * time_delta / 1e6f;
                // Cap to max angular acceleration
                steering_ang = std::clamp(steering_ang, -angular_speed_delta_max, angular_speed_delta_max);
                return speed_ang_current_considered + steering_ang;
            }());

        // 3. Apply command.

        Serial.print(";command=");
        command_speed.print();

        speed_command_last_absolute = command_speed;
        command_motors_for_local_speed(
            motors, odometry,
            odometry->speed_absolute_to_local(command_speed));

        return distance_left;
    }
};

#endif
