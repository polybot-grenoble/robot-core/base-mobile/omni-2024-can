# Comment calculer l'odometrie local de la base mobile ?

[[_TOC_]]

## Représentation géométrique
Dans un premier temps, il est utile de réalisé un schéma, un plan ou une définition géometrique de la base mobile. Comme sur la figure 1 suivante :
| ![space-1.jpg](/images_md/GEOGEBRA_BASEMOBILE.png) | 
|:--:| 
| *Figure 1 : Représentation géométrique de la base mobile sur Géogebra* |

## Calcul des degrès de liberté

Puis pour chaque degré de liberté, X,Y et la rotation en Z. Itérer sur l'impact de tous les moteurs en local. <br>

## Calcul d'interpolation de cercle sur le déplacement de la base mobile

$
(1)\;\;\;\;\;\;\;\;x'=L cos(\theta/2)
$
\
$
(2)\;\;\;\;\;\;\;\;y'=L sin(\theta/2)
$

$
(3)\;\;\;\;\;\;\;\;L = 2 |xy| \frac{sin(\theta/2)}{\theta}
$

$
(3)-(1)\;\;\;\;\;\;\;\;x'=|xy| \frac{1-cos(\theta)}{\theta}
$

$
(3)-(2)\;\;\;\;\;\;\;\;x'=|xy| \frac{sin(\theta)}{\theta}
$

Pour des raisons pratique et éviter la division par 0, on peut utilisé ce développement limité:\
* [calcul pour x'](https://www.desmos.com/calculator/57f5jj1wlc)
* [calcul pour y'](https://www.desmos.com/calculator/xpue3pu3jn)

